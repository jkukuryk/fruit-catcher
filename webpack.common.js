const HTMLWebPackPlugin = require("html-webpack-plugin");
const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
  entry: path.join(__dirname, "/src/index.ts"),
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(png|jpe?g|svg)$/,
        loader: "file-loader",
        options: {
          name: "images/[name].[ext]",
        },
      },
      {
        test: /\.mp3$/,
        loader: "file-loader",
        options: {
          name: "sounds/[name].[ext]",
        },
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HTMLWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html",
      title: "Production",
    }),
  ],
  devServer: {
    hot: true,
    contentBase: "./",
    historyApiFallback: true,
  },
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist"),
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
    alias: {
      assets: path.resolve(__dirname, "./src/assets"),
      classes: path.resolve(__dirname, "./src/classes"),
      stages: path.resolve(__dirname, "./src/stages"),
      sounds: path.resolve(__dirname, "./src/sounds"),
      constants: path.resolve(__dirname, "./src/constants"),
      src: path.resolve(__dirname, "./src"),
    },
  },
};
