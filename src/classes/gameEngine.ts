import * as PIXI from "pixi.js";
import { GameClass, GameScene } from "constants/globalTypes";
import { imageManager } from "classes/imageManager";
import { inputHandler } from "classes/inputClass";
import { ActionType } from "src/constants/actionTypes";

const GAME_WIDTH = 700;
const GAME_HEIGHT = 500;

class Game {
  isWebGL: boolean;
  app: PIXI.Application;
  stages: GameClass[];
  gameSize: { width: number; height: number };
  scene: GameScene;
  won: boolean;

  constructor() {
    this.isWebGL = PIXI.utils.isWebGLSupported();
    this.scene = GameScene.START;
    this._bindMethods(this);
    this.won = false;
  }

  public Start() {
    this.app = new PIXI.Application({
      width: GAME_WIDTH,
      height: GAME_HEIGHT,
    });
    this.gameSize = { width: GAME_WIDTH, height: GAME_HEIGHT };
    document.getElementById("game_container").innerHTML = "";
    document.getElementById("game_container").appendChild(this.app.view);
    this.CenterView();
    this.stages.forEach((stage) => {
      if (stage.Start) {
        stage.Start();
      }
    });
  }

  public Emit(type: ActionType, input?: { [key: string]: any } | null) {
    this.stages.forEach((stage) => {
      if (stage.Action) {
        stage.Action(type, input);
      }
    });
    this.Action(type);
  }
  private Action(type: ActionType, input?: { [key: string]: any } | null) {
    switch (type) {
      case ActionType.KEYDOWN:
        if (this.scene === GameScene.GAME_OVER) {
          this.won = false;
          this.scene = GameScene.START;
          this.Emit(ActionType.CHANGE_SCENE, { scene: GameScene.START });
        } else if (this.scene === GameScene.START) {
          this.scene = GameScene.PLAY;
          this.Emit(ActionType.CHANGE_SCENE, { scene: GameScene.PLAY });
        }
        break;
      case ActionType.GAME_OVER:
        this.scene = GameScene.GAME_OVER;
        this.Emit(ActionType.CHANGE_SCENE, { scene: GameScene.GAME_OVER });
        break;
      case ActionType.PLAYER_WIN:
        this.won = true;
        break;
    }
  }

  public CenterView() {
    this.gameSize = { width: GAME_WIDTH, height: GAME_HEIGHT };

    const minSizeW = Math.min(window.innerWidth, GAME_WIDTH);
    const minSizeH = Math.min(window.innerHeight, GAME_HEIGHT);

    this.gameSize = { width: minSizeW, height: minSizeH };
    this.app.renderer.resize(minSizeW, minSizeH);
  }
  private _bindMethods(self: Game) {
    this.CenterView = this.CenterView.bind(self);
    this.RegisterStages = this.RegisterStages.bind(self);
  }
  public RegisterStages(stages: GameClass[]) {
    this.stages = stages;
  }
}

export const GameInstance = new Game();

export function start() {
  window.addEventListener("resize", GameInstance.CenterView, true);
  window.addEventListener("keydown", inputHandler.keyDown, true);
  window.addEventListener("keyup", inputHandler.keyUp, true);

  imageManager.loadImages();
  GameInstance.Start();

  GameInstance.app.ticker.add(() => {
    GameInstance.app.stage.children.sort(
      (a: any, b: any) => a.zIndex - b.zIndex
    );
  });
}
