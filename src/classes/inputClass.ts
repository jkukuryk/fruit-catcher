import { GameInstance } from "./gameEngine";
import { ActionType } from "constants/actionTypes";

const LEFT_CODES = ["ArrowLeft", "KeyA"];
const RIGHT_CODES = ["ArrowRight", "KeyD"];

class InputClass {
  pressedKeys: string[];

  constructor() {
    this.pressedKeys = [];
    this.keyUp = this.keyUp.bind(this);
    this.keyDown = this.keyDown.bind(this);
  }
  public keyUp(e: KeyboardEvent) {
    const keyCode = e.code;
    this.removeKeys([keyCode]);
  }

  public keyDown(e: KeyboardEvent) {
    const keyCode = e.code;
    if (LEFT_CODES.includes(keyCode) || RIGHT_CODES.includes(keyCode)) {
      this.removeKeys([...LEFT_CODES, , ...RIGHT_CODES]);
    }
    this.addKey(keyCode);
    GameInstance.Emit(ActionType.KEYDOWN, { keyCode });
  }

  private addKey(code: string) {
    this.pressedKeys.push(code);
  }

  private removeKeys(keyCodes: string[]) {
    this.pressedKeys = this.pressedKeys.filter(
      (keyCode) => !keyCodes.includes(keyCode)
    );
  }

  public getHorizontalInput() {
    const leftIndex = this.pressedKeys.findIndex((key) =>
      LEFT_CODES.includes(key)
    );
    const rightIndex = this.pressedKeys.findIndex((key) =>
      RIGHT_CODES.includes(key)
    );
    if (leftIndex === -1 && rightIndex === -1) {
      return 0;
    }
    return leftIndex > rightIndex ? -1 : 1;
  }
}
export const inputHandler = new InputClass();

export const getHorizontalInput = () => {
  return inputHandler.getHorizontalInput();
};
