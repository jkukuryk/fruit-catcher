import { GameInstance } from "./gameEngine";
import * as PIXI from "pixi.js";

export type DrawRect = {
  x: number;
  y: number;
  width: number;
  height: number;
};

export type DrawImageInput = {
  x?: number;
  y?: number;
  rotation?: number;
};

type ImageList = {
  [key: string]: {
    isLoaded: boolean;
    src: string;
    name: string;
    isRendered: boolean;
    sprite: PIXI.Sprite | null;
  };
};

class ImageManager {
  images: ImageList;
  isLoading: boolean;

  constructor() {
    this.images = {} as ImageList;
    this.canRender = this.canRender.bind(this);
    this.loadImages = this.loadImages.bind(this);
  }

  public loadImages() {
    const loader = new PIXI.Loader();

    Object.keys(this.images).forEach((imageAlias: string) => {
      const image = this.images[imageAlias];
      loader.add(image.name, image.src);
    });

    loader.load((loader: PIXI.Loader) => {
      Object.keys(loader.resources).forEach((imageAlias: string) => {
        const sprite = new PIXI.Sprite(loader.resources[imageAlias].texture);
        this.images[imageAlias].sprite = sprite;
        this.images[imageAlias].isLoaded = true;
      });
    });
  }

  public add(name: string, src: string) {
    this.images[name] = {
      isLoaded: false,
      src,
      name,
      sprite: null,
      isRendered: false,
    };
  }
  public canRender(name: string) {
    return this.images[name].isLoaded;
  }

  public get(alias: string) {
    if (this.images[alias]) {
      return this.images[alias];
    }
    return null;
  }

  public getTexture(alias: string) {
    const img = this.get(alias);
    if (img) {
      return PIXI.Texture.from(img.src);
    }
    return null;
  }

  public removeSprite(sprite: PIXI.Sprite) {
    sprite.x = -999;
  }

  public draw(
    spriteName: string,
    container: PIXI.Container,
    { x = 0, y = 0 }: DrawImageInput,
    rect?: DrawRect
  ) {
    const image = this.images[spriteName];
    if (image?.isLoaded) {
      var texture = this.getTexture(spriteName);
      const currentSprite = new PIXI.Sprite(texture);

      if (!!rect) {
        const graphics = new PIXI.Graphics();
        graphics.beginFill(0xffffff);
        graphics.drawRect(x, y, rect.width, rect.height);
        graphics.endFill();
        currentSprite.mask = graphics;
        container.addChild(currentSprite);
        currentSprite.position.set(x - rect.x, y - rect.y);
      } else {
        container.addChild(currentSprite);
        currentSprite.position.set(x, y);
      }
    }
  }
}

export const imageManager = new ImageManager();
