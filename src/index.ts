import "reset-css";
import { start, GameInstance } from "classes/gameEngine";
import { Player } from "src/stages/player";
import { FallingFood } from "src/stages/fallingFood";
import { Background } from "src/stages/backgound";
import { Score } from "src/stages/score";
import { Overlay } from "src/stages/overlay";

GameInstance.RegisterStages([
  new Player(),
  new FallingFood(),
  new Background(),
  new Score(),
  new Overlay(),
]);
start();
