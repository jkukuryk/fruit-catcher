import { imageManager } from "classes/imageManager";
import foodScore from "assets/foodScore.png";
import numbers from "assets/numbers.png";
import { GameInstance } from "src/classes/gameEngine";
import { ZINDEX } from "constants/zindex";
import * as PIXI from "pixi.js";
import { ActionType } from "src/constants/actionTypes";
import { GameScene } from "src/constants/globalTypes";

const NUMBERS_CELL_WIDTH = 22;
const NUMBERS_CELL_HEIGHT = 16;
const WIN_CONDITION = 10;

export class Score {
  score: number;

  constructor() {
    imageManager.add("foodScore", foodScore);
    imageManager.add("numbers", numbers);
    this.score = 0;
  }
  public Action(action: ActionType, data?: any) {
    switch (action) {
      case ActionType.ADD_POINT:
        this.score++;
        if (this.score >= WIN_CONDITION) {
          GameInstance.Emit(ActionType.PLAYER_WIN);
        }
        break;
      case ActionType.CHANGE_SCENE:
        this.score = 0;
    }
  }
  public Start() {
    const container = new PIXI.Container();

    GameInstance.app.ticker.add(() => {
      container.removeChildren();
      if (GameInstance.scene === GameScene.PLAY) {
        GameInstance.app.stage.addChild(container);
        imageManager.draw("foodScore", container, {
          x: 20,
          y: 20,
        });
        const tensNumber = Math.floor(this.score / 10);
        const singleNumber = this.score % 10;

        if (tensNumber) {
          imageManager.draw(
            "numbers",
            container,
            { x: 60, y: 22 },
            {
              x: NUMBERS_CELL_WIDTH * tensNumber,
              y: 0,
              width: NUMBERS_CELL_WIDTH,
              height: NUMBERS_CELL_HEIGHT,
            }
          );
          imageManager.draw(
            "numbers",
            container,
            { x: 84, y: 22 },
            {
              x: NUMBERS_CELL_WIDTH * singleNumber,
              y: 0,
              width: NUMBERS_CELL_WIDTH,
              height: NUMBERS_CELL_HEIGHT,
            }
          );
        } else {
          imageManager.draw(
            "numbers",
            container,
            { x: 60, y: 22 },
            {
              x: NUMBERS_CELL_WIDTH * singleNumber,
              y: 0,
              width: NUMBERS_CELL_WIDTH,
              height: NUMBERS_CELL_HEIGHT,
            }
          );
        }
      }
      container.zIndex = ZINDEX.layout;
    });
  }
}
