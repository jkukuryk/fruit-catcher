import { imageManager } from "classes/imageManager";
import foodImage from "assets/foodSprite.png";
import { GameInstance } from "src/classes/gameEngine";

import * as PIXI from "pixi.js";
import { ActionType } from "src/constants/actionTypes";
import { ZINDEX } from "src/constants/zindex";
import { GameScene } from "src/constants/globalTypes";

const FOOD_CELL_WIDTH = 16;
const FOOD_CELL_HEIGHT = 16;
const SPRITE_CELLS_HORIZONTAL = 8;
const SPRITE_CELLS_VERTICAL = 8;
const SCREEN_MARGIN = 50;

type FoodPosition = {
  x: number;
  y: number;
  spriteX: number;
  spriteY: number;
  visible: boolean;
};

export class FallingFood {
  foodsItems: FoodPosition[];
  acceleration: number;
  foodsTotal: number;
  lastTime: number;
  nextTime: number;
  speed: number;
  translateY: number;

  constructor() {
    imageManager.add("food", foodImage);
    this.foodsItems = [];
    this.foodsTotal = 42;
    this.nextTime = 0;
    this.speed = 3;
    this.translateY = 0;
    this._reset = this._reset.bind(this);
  }
  private _reset() {
    this.translateY = 0;

    this.foodsItems.forEach((food, key) => {
      this.foodsItems[key].visible = true;

      const spriteX =
        Math.floor(Math.random() * SPRITE_CELLS_HORIZONTAL) * FOOD_CELL_WIDTH;
      const spriteY =
        Math.floor(Math.random() * SPRITE_CELLS_VERTICAL) * FOOD_CELL_HEIGHT;

      const positionX =
        SCREEN_MARGIN +
        Math.round(
          Math.random() * (GameInstance.gameSize.width - SCREEN_MARGIN * 2)
        );

      this.foodsItems[key].spriteX = spriteX;
      this.foodsItems[key].spriteY = spriteY;
      this.foodsItems[key].x = positionX;
    });
  }

  public Start() {
    const containers = this._generateFoodGrid();

    this.foodsItems.forEach((food, key) => {
      GameInstance.app.stage.addChild(containers[key]);
      containers[key].zIndex = ZINDEX.food;
    });

    GameInstance.app.ticker.add((delta) => {
      if (GameInstance.scene === GameScene.PLAY) {
        this.foodsItems.forEach((food, key) => {
          containers[key].removeChildren();
          if (food.visible) {
            imageManager.draw(
              "food",
              containers[key],
              { x: food.x, y: food.y + this.translateY },
              {
                x: food.spriteX,
                y: food.spriteY + this.translateY,
                width: FOOD_CELL_WIDTH,
                height: FOOD_CELL_HEIGHT,
              }
            );
            containers[key].position.y = this.translateY;
            containers[key].position.x = 0;
            if (
              food.y - food.spriteY + this.translateY >
              GameInstance.gameSize.height
            ) {
              this.foodsItems[key].visible = false;
            }
          }
        });
        this.speed += 0.001;
        this.translateY += this.speed;
        if (this.foodsItems.findIndex((food) => food.visible) === -1) {
          GameInstance.Emit(ActionType.GAME_OVER);
        }
      }
    });
  }

  private _generateFoodGrid() {
    let nextFoodItemMargin = 260;
    let nextFoodPosition = 0;
    const containers = [] as PIXI.Container[];

    for (let i = 0; i < this.foodsTotal; i++) {
      const container = new PIXI.Container();
      containers.push(container);

      const spriteX =
        Math.floor(Math.random() * SPRITE_CELLS_HORIZONTAL) * FOOD_CELL_WIDTH;
      const spriteY =
        Math.floor(Math.random() * SPRITE_CELLS_VERTICAL) * FOOD_CELL_HEIGHT;

      const positionX =
        SCREEN_MARGIN +
        Math.round(
          Math.random() * (GameInstance.gameSize.width - SCREEN_MARGIN * 2)
        );
      const positionY = nextFoodPosition;

      this.foodsItems.push({
        x: positionX,
        y: positionY,
        spriteX,
        spriteY,
        visible: true,
      });

      nextFoodPosition -= nextFoodItemMargin;
      nextFoodItemMargin -= 6;
      if (nextFoodItemMargin < 10) {
        nextFoodItemMargin = 10;
      }
    }
    return containers;
  }

  public Action(action: ActionType, data: { [key: string]: any }) {
    switch (action) {
      case ActionType.CHANGE_SCENE:
        this._reset();
        break;
      case ActionType.PLAYER_UPDATE_POSITION:
        const { x, y } = data;
        const collisionMargin = 25;

        const foodIndex = this.foodsItems.findIndex((food) => {
          const foodX = food.x + FOOD_CELL_WIDTH / 2;
          const foodY = food.y - FOOD_CELL_HEIGHT / 2 + this.translateY;
          if (Math.abs(foodX - x) < collisionMargin) {
            if (foodY > y && foodY < y + 80) {
              return true;
            }
          }
          return false;
        });

        if (foodIndex > -1 && this.foodsItems[foodIndex].visible) {
          this.foodsItems[foodIndex].visible = false;
          GameInstance.Emit(ActionType.ADD_POINT);
        }
    }
  }
}
