import { imageManager } from "classes/imageManager";
import mainBackground from "assets/mainBackground.png";
import grass from "assets/grass.png";
import { GameInstance } from "src/classes/gameEngine";
import { ZINDEX } from "constants/zindex";
import * as PIXI from "pixi.js";
import { GameScene } from "src/constants/globalTypes";

export class Background {
  constructor() {
    imageManager.add("mainBackground", mainBackground);
    imageManager.add("grass", grass);
  }

  public Start() {
    const backGroundContainer = new PIXI.Container();
    const grassContainer = new PIXI.Container();
    GameInstance.app.ticker.add(() => {
      backGroundContainer.removeChildren();
      grassContainer.removeChildren();
      GameInstance.app.stage.addChild(backGroundContainer);
      GameInstance.app.stage.addChild(grassContainer);

      imageManager.draw("mainBackground", backGroundContainer, {
        x: 0,
        y: 0,
      });

      backGroundContainer.zIndex = ZINDEX.background;

      imageManager.draw("grass", grassContainer, {
        x: 0,
        y: GameInstance.gameSize.height - 30,
      });

      grassContainer.zIndex = ZINDEX.beforePlayer;
    });
  }
}
