import { GameInstance } from "classes/gameEngine";
import { imageManager } from "classes/imageManager";
import { getHorizontalInput } from "classes/inputClass";
import player from "assets/playerSprite.png";
import { ActionType } from "src/constants/actionTypes";
import { ZINDEX } from "src/constants/zindex";
import * as PIXI from "pixi.js";
import { GameScene } from "src/constants/globalTypes";

const PLAYER_CELL_WIDTH = 84;
const PLAYER_CELL_HEIGHT = 84;
const PLAYER_MARGIN = 12;
const FPS = 12;
const frameChange = FPS / 60;

export class Player {
  x: number;
  y: number;
  speed: number;
  runFrame: number;
  maxSpeed: number;
  direction: number;
  acceleration: number;
  container: PIXI.Container;

  constructor() {
    imageManager.add("player", player);
    this.y = -999;
    this.x = 0;
    this.container = new PIXI.Container();
    this.speed = 0;
    this.maxSpeed = 8;
    this.acceleration = 1;
    this.runFrame = 0;
    this._getRunFrame = this._getRunFrame.bind(this);
  }

  public Start() {
    GameInstance.app.stage.addChild(this.container);
    this.x = GameInstance.gameSize.width / 2 - PLAYER_CELL_WIDTH / 2;

    GameInstance.app.ticker.add((delta) => {
      this.Update(delta);
    });
  }
  public Update(delta: number) {
    const isGame = GameInstance.scene === GameScene.PLAY;
    this.y = GameInstance.gameSize.height - PLAYER_CELL_HEIGHT - 10;
    const input = isGame ? getHorizontalInput() : 0;

    if (input !== 0) {
      const newSpeed = this.speed + this.acceleration * input;
      if (Math.abs(newSpeed) < this.maxSpeed) {
        this.speed = newSpeed;
      }
    } else if (this.speed > 1) {
      this.speed--;
    } else if (this.speed < -1) {
      this.speed++;
    } else {
      this.speed = 0;
    }
    if (isGame) {
      this.x += this.speed * delta;
    } else {
      this.x = GameInstance.gameSize.width / 2 - PLAYER_CELL_WIDTH / 2;
    }
    if (this.x > GameInstance.gameSize.width - PLAYER_CELL_WIDTH / 2) {
      this.x = GameInstance.gameSize.width - PLAYER_CELL_WIDTH / 2;
      this.speed = 0;
    }
    if (this.x < -PLAYER_CELL_WIDTH / 2) {
      this.x = -PLAYER_CELL_WIDTH / 2;
      this.speed = 0;
    }
    this.runFrame += frameChange;

    const runFrame = this._getRunFrame();
    this.container.zIndex = ZINDEX.player;
    this.container.removeChildren();
    imageManager.draw(
      "player",
      this.container,
      {
        x: this.x,
        y: this.y,
      },
      {
        x: PLAYER_CELL_WIDTH * runFrame.x,
        y: PLAYER_CELL_HEIGHT * runFrame.y,
        width: PLAYER_CELL_WIDTH,
        height: PLAYER_CELL_HEIGHT,
      }
    );
    GameInstance.Emit(ActionType.PLAYER_UPDATE_POSITION, {
      x: this.x + PLAYER_CELL_WIDTH / 2,
      y: this.y,
    });
  }

  private _getRunFrame() {
    const framesIDLE = [
      { x: 0, y: 0 },
      { x: 1, y: 0 },
      { x: 2, y: 0 },
      { x: 3, y: 0 },
    ];
    const framesRIGHT = [
      { x: 7, y: 1 },
      { x: 0, y: 2 },
      { x: 2, y: 2 },
      { x: 3, y: 2 },
    ];
    const framesLEFT = [
      { x: 5, y: 2 },
      { x: 6, y: 2 },
      { x: 0, y: 3 },
      { x: 1, y: 3 },
    ];

    let runTable = framesIDLE;
    if (this.speed !== 0) {
      runTable = this.speed > 0 ? framesRIGHT : framesLEFT;
    }
    const framesLimit = runTable.length;
    if (this.runFrame >= framesLimit) {
      this.runFrame = 0;
    }
    const currentFrame = Math.floor(this.runFrame);
    return runTable[currentFrame];
  }
}
