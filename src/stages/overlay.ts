import { imageManager } from "classes/imageManager";
import titlePng from "assets/title.png";
import winPng from "assets/win.png";
import losePng from "assets/lose.png";
import { GameInstance } from "src/classes/gameEngine";
import { ZINDEX } from "constants/zindex";
import * as PIXI from "pixi.js";
import { GameScene } from "src/constants/globalTypes";

const GAME_TITLE_WIDTH = 294;

export class Overlay {
  constructor() {
    imageManager.add("title", titlePng);
    imageManager.add("win", winPng);
    imageManager.add("lose", losePng);
  }

  public Start() {
    const titleContainer = new PIXI.Container();
    GameInstance.app.stage.addChild(titleContainer);
    const winContainer = new PIXI.Container();
    GameInstance.app.stage.addChild(winContainer);
    const loseContainer = new PIXI.Container();
    GameInstance.app.stage.addChild(loseContainer);
    titleContainer.zIndex = ZINDEX.layout;
    winContainer.zIndex = ZINDEX.layout;
    loseContainer.zIndex = ZINDEX.layout;

    GameInstance.app.ticker.add(() => {
      titleContainer.removeChildren();
      winContainer.removeChildren();
      loseContainer.removeChildren();
      if (GameInstance.scene === GameScene.START) {
        imageManager.draw("title", titleContainer, {
          x: GameInstance.gameSize.width / 2 - GAME_TITLE_WIDTH / 2,
          y: 90,
        });
        titleContainer.zIndex = ZINDEX.layout;
      }
      if (GameInstance.scene === GameScene.GAME_OVER) {
        if (GameInstance.won) {
          imageManager.draw("win", winContainer, {
            x: GameInstance.gameSize.width / 2 - GAME_TITLE_WIDTH / 2,
            y: 90,
          });
        } else {
          imageManager.draw("lose", loseContainer, {
            x: GameInstance.gameSize.width / 2 - GAME_TITLE_WIDTH / 2,
            y: 90,
          });
        }
      }
    });
  }
}
