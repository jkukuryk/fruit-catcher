import { ActionType } from "./actionTypes";

export type GameClass = {
  Start?: () => void;
  Action?: (type: ActionType, data: any) => void;
};

export enum GameScene {
  START = "START",
  PLAY = "PLAY",
  GAME_OVER = "GAME_OVER",
}
